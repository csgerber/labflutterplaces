import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:proyelp/model/yelp_response.dart';

class MenuWidget extends StatelessWidget {
  final String field;
  final Function onTapResult;

  //constructor
  MenuWidget(this.field, this.onTapResult);


  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () => onTapResult(field),
        child: Container(
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(bottom: 10),

            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  color: Color.fromARGB(255, 255, 0, 0),
                  margin: EdgeInsets.only(right: 10),
                  child: Text(field),
                )
              ],
            )));
  }
}
