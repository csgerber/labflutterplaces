// This example shows a [Scaffold] with an [AppBar], a [BottomAppBar] and a
// [FloatingActionButton]. The [body] is a [Text] placed in a [Center] in order
// to center the text within the [Scaffold] and the [FloatingActionButton] is
// centered and docked within the [BottomAppBar] using
// [FloatingActionButtonLocation.centerDocked]. The [FloatingActionButton] is
// connected to a callback that increments a counter.

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyelp/widgets/name_widget.dart';
import 'package:proyelp/widgets/menu_widget.dart';

import 'package:proyelp/screens/my_search.dart';
import 'package:proyelp/model/yelp_response.dart';
import 'package:random_string/random_string.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Code Sample for material.Scaffold',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyStatefulWidget(),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  //notice that this Stateful Widget overrides the createState method which returns of type State<MyStatefulWidget>
  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

//the underscore means that this is private to this class
class _MyStatefulWidgetState extends State<MyStatefulWidget> {

  //member of my class
  //List<BusinessesListBean> businesses = [];
  List<String> names = ["Hello", "Goodbye"];



  //method calld addBusiness, which will modify
//  addBusiness() async {
//    String name = await Navigator.push(context,
//        CupertinoPageRoute(builder: (BuildContext context) {
//      return SearchScreen();
//    }));
//    setState(() {
//      names.add(name);
//    });
//  }

  Future<void> addName() async {
    //capture the string from the MySearchScreen
        String name = await Navigator.push(context,
        CupertinoPageRoute(builder: (BuildContext context) {
          //this works because my search screen is ultimately returning a string
      return MySearchScreen();
    }));

    //next, we call the setState method of the State object and mutate the array
    setState(() {
      names.add(name);
    });


  }

   printName(name) {
    debugPrint(name);
  }


  //these "State" classes also have a build method that returns a Widget.
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            MenuWidget('One', printName),
            MenuWidget('Two',  printName),
            MenuWidget('Three',  printName),
            MenuWidget('Four',  printName),

          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Sample Code'),
      ),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return NameWidget(names[index], null);
        },
        itemCount: names.length,
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 50.0,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        //this syntax is the same as fat-arrow
        //onPressed: () { addName();},
        onPressed: () => addName(),
        tooltip: 'Increment Counter',
        child: Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
