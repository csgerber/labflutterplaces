
class Location {

  final String address1;
  final Object address2;
  final String address3;
  final String city;
  final String zip_code;
  final String country;
  final String state;
  final List<String> display_address;

	Location.fromJsonMap(Map<String, dynamic> map): 
		address1 = map["address1"],
		address2 = map["address2"],
		address3 = map["address3"],
		city = map["city"],
		zip_code = map["zip_code"],
		country = map["country"],
		state = map["state"],
		display_address = List<String>.from(map["display_address"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['address1'] = address1;
		data['address2'] = address2;
		data['address3'] = address3;
		data['city'] = city;
		data['zip_code'] = zip_code;
		data['country'] = country;
		data['state'] = state;
		data['display_address'] = display_address;
		return data;
	}
}
