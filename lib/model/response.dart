import 'package:proyelp/model/businesses.dart';
import 'package:proyelp/model/region.dart';

class MyResponse {

  final List<Business> businesses;
  final int total;
  final Region region;


	MyResponse.fromJsonMap(Map<String, dynamic> map):
		businesses = List<Business>.from(map["businesses"].map((it) => Business.fromJsonMap(it))),
		total = map["total"],
		region = Region.fromJsonMap(map["region"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['businesses'] = businesses != null ? 
			this.businesses.map((v) => v.toJson()).toList()
			: null;
		data['total'] = total;
		data['region'] = region == null ? null : region.toJson();
		return data;
	}
}
