import 'package:proyelp/model/categories.dart';
import 'package:proyelp/model/coordinates.dart';
import 'package:proyelp/model/location.dart';

class Business {

  final String id;
  final String alias;
  final String name;
  final String image_url;
  final bool is_closed;
  final String url;
  final int review_count;
  final List<Categories> categories;
  final double rating;
  final Coordinates coordinates;
  final List<Object> transactions;
  final String price;
  final Location location;
  final String phone;
  final String display_phone;
  final double distance;

	Business.fromJsonMap(Map<String, dynamic> map):
		id = map["id"],
		alias = map["alias"],
		name = map["name"],
		image_url = map["image_url"],
		is_closed = map["is_closed"],
		url = map["url"],
		review_count = map["review_count"],
		categories = List<Categories>.from(map["categories"].map((it) => Categories.fromJsonMap(it))),
		rating = map["rating"],
		coordinates = Coordinates.fromJsonMap(map["coordinates"]),
		transactions = map["transactions"],
		price = map["price"],
		location = Location.fromJsonMap(map["location"]),
		phone = map["phone"],
		display_phone = map["display_phone"],
		distance = map["distance"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['alias'] = alias;
		data['name'] = name;
		data['image_url'] = image_url;
		data['is_closed'] = is_closed;
		data['url'] = url;
		data['review_count'] = review_count;
		data['categories'] = categories != null ?
			this.categories.map((v) => v.toJson()).toList()
			: null;
		data['rating'] = rating;
		data['coordinates'] = coordinates == null ? null : coordinates.toJson();
		data['transactions'] = transactions;
		data['price'] = price;
		data['location'] = location == null ? null : location.toJson();
		data['phone'] = phone;
		data['display_phone'] = display_phone;
		data['distance'] = distance;
		return data;
	}
}
