
//you should use this plugin Json2Dart in order to generate this.
//http://opensource.adobe.com/Spry/samples/data_region/JSONDataSetSample.html
/*

{
"id": "0001",
"type": "donut",
"name": "Cake",
"ppu": 0.55,
"batters":
{
"batter":
[
{ "id": "1001", "type": "Regular" },
{ "id": "1002", "type": "Chocolate" },
{ "id": "1003", "type": "Blueberry" },
{ "id": "1004", "type": "Devil's Food" }
]
},
"topping":
[
{ "id": "5001", "type": "None" },
{ "id": "5002", "type": "Glazed" },
{ "id": "5005", "type": "Sugar" },
{ "id": "5007", "type": "Powdered Sugar" },
{ "id": "5006", "type": "Chocolate with Sprinkles" },
{ "id": "5003", "type": "Chocolate" },
{ "id": "5004", "type": "Maple" }
]
}

*/



class YelpResponse {
  int total;
  RegionBean region;
  List<BusinessesListBean> businesses;

  YelpResponse({this.total, this.region, this.businesses});

  YelpResponse.fromJson(Map<String, dynamic> json) {
    this.total = json['total'];
    this.region =
        json['region'] != null ? RegionBean.fromJson(json['region']) : null;
    this.businesses = (json['businesses'] as List) != null
        ? (json['businesses'] as List)
            .map((i) => BusinessesListBean.fromJson(i))
            .toList()
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.region != null) {
      data['region'] = this.region.toJson();
    }
    data['businesses'] = this.businesses != null
        ? this.businesses.map((i) => i.toJson()).toList()
        : null;
    return data;
  }
}

class RegionBean {
  CenterBean center;

  RegionBean({this.center});

  RegionBean.fromJson(Map<String, dynamic> json) {
    this.center =
        json['center'] != null ? CenterBean.fromJson(json['center']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.center != null) {
      data['center'] = this.center.toJson();
    }
    return data;
  }
}

class BusinessesListBean {
  String price;
  String phone;
  String id;
  String alias;
  String name;
  String url;
  String imageUrl;
  bool isClosed;
  double distance;
  double rating;
  int reviewCount;
  CoordinatesBean coordinates;
  LocationBean location;
  List<String> transactions;
  List<CategoriesListBean> categories;

  BusinessesListBean(
      {this.price,
      this.phone,
      this.id,
      this.alias,
      this.name,
      this.url,
      this.imageUrl,
      this.isClosed,
      this.distance,
      this.rating,
      this.reviewCount,
      this.coordinates,
      this.location,
      this.categories,
      this.transactions});

  BusinessesListBean.fromJson(Map<String, dynamic> json) {
    this.price = json['price'];
    this.phone = json['phone'];
    this.id = json['id'];
    this.alias = json['alias'];
    this.name = json['name'];
    this.url = json['url'];
    this.imageUrl = json['image_url'];
    this.isClosed = json['is_closed'];
    this.distance = json['distance'];
    this.rating = json['rating'];
    this.reviewCount = json['review_count'];
    this.coordinates = json['coordinates'] != null
        ? CoordinatesBean.fromJson(json['coordinates'])
        : null;
    this.location = json['location'] != null
        ? LocationBean.fromJson(json['location'])
        : null;
    this.categories = (json['categories'] as List) != null
        ? (json['categories'] as List)
            .map((i) => CategoriesListBean.fromJson(i))
            .toList()
        : null;

    List<dynamic> transactionsList = json['transactions'];
    this.transactions = new List();
    this.transactions.addAll(transactionsList.map((o) => o.toString()));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['phone'] = this.phone;
    data['id'] = this.id;
    data['alias'] = this.alias;
    data['name'] = this.name;
    data['url'] = this.url;
    data['image_url'] = this.imageUrl;
    data['is_closed'] = this.isClosed;
    data['distance'] = this.distance;
    data['rating'] = this.rating;
    data['review_count'] = this.reviewCount;
    if (this.coordinates != null) {
      data['coordinates'] = this.coordinates.toJson();
    }
    if (this.location != null) {
      data['location'] = this.location.toJson();
    }
    data['categories'] = this.categories != null
        ? this.categories.map((i) => i.toJson()).toList()
        : null;
    data['transactions'] = this.transactions;
    return data;
  }
}

class CenterBean {
  double latitude;
  double longitude;

  CenterBean({this.latitude, this.longitude});

  CenterBean.fromJson(Map<String, dynamic> json) {
    this.latitude = json['latitude'];
    this.longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}

class CoordinatesBean {
  double latitude;
  double longitude;

  CoordinatesBean({this.latitude, this.longitude});

  CoordinatesBean.fromJson(Map<String, dynamic> json) {
    this.latitude = json['latitude'];
    this.longitude = json['longitude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }
}

class LocationBean {
  String city;
  String country;
  String address2;
  String address3;
  String state;
  String address1;
  String zipCode;

  LocationBean(
      {this.city,
      this.country,
      this.address2,
      this.address3,
      this.state,
      this.address1,
      this.zipCode});

  LocationBean.fromJson(Map<String, dynamic> json) {
    this.city = json['city'];
    this.country = json['country'];
    this.address2 = json['address2'];
    this.address3 = json['address3'];
    this.state = json['state'];
    this.address1 = json['address1'];
    this.zipCode = json['zip_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['city'] = this.city;
    data['country'] = this.country;
    data['address2'] = this.address2;
    data['address3'] = this.address3;
    data['state'] = this.state;
    data['address1'] = this.address1;
    data['zip_code'] = this.zipCode;
    return data;
  }
}

class CategoriesListBean {
  String alias;
  String title;

  CategoriesListBean({this.alias, this.title});

  CategoriesListBean.fromJson(Map<String, dynamic> json) {
    this.alias = json['alias'];
    this.title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['alias'] = this.alias;
    data['title'] = this.title;
    return data;
  }
}
