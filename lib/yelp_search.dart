import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:proyelp/constants.dart';
import 'package:proyelp/model/response.dart';

class YelpSearch {
  static YelpSearch instance = YelpSearch();
  Dio client = Dio(BaseOptions(
      headers: {"Authorization": "Bearer $YELP_FUSION_KEY"},
      baseUrl: "https://api.yelp.com/v3"));
  Future<MyResponse> search(String term, String location) async {
    print("term: $term");
    print("location: $location");
    try {
      print("requesting");
      Response response = await client
          .get("/businesses/search?term=$term&radius=1000&location=$location");
      print('response');
      print(response);
      MyResponse data = new MyResponse.fromJsonMap(response.data);
      return data;
    } catch (e) {
      print(e);
      return null;
    }
  }
}
