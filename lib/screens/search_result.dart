import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:proyelp/widgets/business_widget.dart';
import 'package:proyelp/model/yelp_response.dart';
import 'package:proyelp/model/response.dart';
import 'package:proyelp/model/businesses.dart';
import 'package:proyelp/yelp_search.dart';

class SearchResult extends StatelessWidget {
  final String location;
  final String term;
  SearchResult({this.location, this.term});

  Widget renderResult(MyResponse response) {
    return ListView.builder(
        itemBuilder: (BuildContext context, index) {
          Business business = response.businesses[index];
          return BusinessWidget(business, (Business business) {
            Navigator.pop(context, business);
          });
        },
        itemCount: response.businesses.length);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Title(
          child: Text("Search Result"),
          color: Colors.white,
        ),
      ),
      body: FutureBuilder(
        future: YelpSearch.instance.search(term, location),
        builder: (BuildContext context, AsyncSnapshot<MyResponse> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.active:
            case ConnectionState.waiting:
            case ConnectionState.none:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return this.renderResult(snapshot.data);
          }
        },
      ),
    );
  }
}
