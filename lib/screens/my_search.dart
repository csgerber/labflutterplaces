import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyelp/screens/search_result.dart';
import 'package:proyelp/model/yelp_response.dart';
import 'package:proyelp/model/businesses.dart';

class MySearchScreen extends StatefulWidget {
  //a stateful widget needs a construdctor
  MySearchScreen();
  //we also need to create a state object.
  @override
  State<MySearchScreen> createState() {
    // TODO: implement createState
    return MySearchScreenState();
  }
}

class MySearchScreenState extends State<MySearchScreen> {

  //here are our members, or model
  String location = "Chicago";
  String term = "House Pub";

  //these are the entities do the changing, or controllers
  TextEditingController termController;
  TextEditingController locationController;

  //this uses M-V-C
  @override
  initState() {
    super.initState();

    termController = TextEditingController(
      text: term,
    );
    termController.addListener(() {
      setState(() {
        term = termController.text;
      });
    });
    locationController = TextEditingController(
      text: location,
    );
    locationController.addListener(() {
      setState(() {
        location = locationController.text;
      });
    });
  }

  Widget termField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(child: Text("Name")),
      TextFormField(
        controller: termController,
        decoration: InputDecoration(hintText: "e.g. Pleasant Hub"),
      )
    ]);
  }

  Widget locationField() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(child: Text("Location")),
      TextFormField(
        controller: locationController,
        decoration: InputDecoration(hintText: "e.g. Bruxelles or Chicago or 98101"),
      )
    ]);
  }

  search() async {
    //this will push a new route onto the stack
    Business business = await Navigator.push(context,
        CupertinoPageRoute(builder: (BuildContext context) {
          return SearchResult(
            location: location,
            term: term,
          );
        }));
    //when this returns, it'll simply skip this page and go back to the grandparent
    Navigator.pop(context, business);
  }

//  setTextValues()  {
//    //this will push a new route onto the stack
//    String name = location + " | " + term;
//    Navigator.pop(context, name);
//  }

  Widget searchButton() {
    return FlatButton(
      color: Colors.blue,
      onPressed: this.search,
      child: Row(
        children: <Widget>[
          Icon(Icons.search),
          Text("Search"),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Title(
          child: Text("Search Places"),
          color: Colors.white,
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(10),
          child: ListView(
            children: <Widget>[
              this.termField(),
              this.locationField(),
              this.searchButton()
            ],
          )),
    );
  }
}
